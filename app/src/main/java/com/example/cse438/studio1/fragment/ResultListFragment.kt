package com.example.cse438.studio1.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.cse438.studio1.R
import kotlinx.android.synthetic.main.fragment_result_list.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container)
    }

    override fun onStart() {
        super.onStart()
        search_for_text.text = "Search for: $this.query"
    }
}